module.exports = {
  server: {
    port: 8989,
    ip: '0.0.0.0'
  },
  db: {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'suyi',
    multipleStatements: true,
    charset: 'utf8_general_ci'
  },
  md5_key: 'AKJDLKAsjdoiqwjdl;aSldaa,dlqlwekl,sadASDdadqthis.q'
}