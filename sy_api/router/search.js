// 引入express并且创建router
const router = require("express").Router()
// 引入数据库
const db = require('../mysql/db')

// 搜素
router.post('/getSeGoods', (req, res) => {
    let page = req.body.page || 1
    let per_page = req.body.per_page || 4
    let offset = (page - 1) * per_page
    let search_name = req.body.search_name
    let sql = `SELECT * FROM sy_index_goods WHERE title LIKE '%${search_name}%' LIMIT ${offset},${per_page}`
    db.query(sql, (error, result) => {
        // console.log(result)
        if (error) {
            console.log(error)
        } else {
            // if (result == []) {
            //     res.json({
            //         status: 400,
            //         msg: '不好意思,我们没有找到您需要的商品'
            //     })
            // } else {
                res.json({
                    status: 200,
                    data: result
                })
            // }
        }
    })
})

module.exports = router