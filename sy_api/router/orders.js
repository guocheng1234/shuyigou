// 引入express并且创建router
const router = require("express").Router()
// 引入数据库
const db = require('../mysql/db')
// md5加密
const md5 = require('md5')
const {
  md5_key
} = require('../config')
const jwt = require('jsonwebtoken')

let takeId = function (token) {
  token = token.substring(6)
  let decode = jwt.verify(token, md5_key)
  // console.log(decode)
  return {
    id
  } = decode
}

// 验证支付密码对错
router.post('/paywords', (req, res) => {
  const paypassword = req.body.pay_password
  // console.log(pay_password)
  let token = req.headers.authorization
  try {
    takeId(token)
    // pay_password = md5(paypassword + md5_key)
    // console.log(takeId(token))
    let sql = 'select pay_password from sy_users where id=?'
    db.query(sql, id, (err, result) => {
      // console.log(result)
      if (result[0].pay_password !== md5(paypassword + md5_key)) {
        res.json({
          status: 400,
          msg: '支付密码错输入错误'
        })
      } else {
        res.json({
          status: 200
        })
      }
    })
  } catch (err) {}
})

// 添加订单
router.post('/orders', (req, res) => {
  let token = req.headers.authorization

  try {
    takeId(token)
    let u_id = id // 用户id
    let goodsIds = req.body.ids // 购买商品id
    let totalArr = req.body.total.split(',') // 购买数量
    let a_id = req.body.a_id // 收获地址id
    let status = req.body.status

    db.query(`select title,price,img,store from sy_pc_goods where id in (${goodsIds});
    select name,tel,province,city,county from sy_address where id =${a_id}`, (err1, data) => {
      // console.log(data);
      // 获取订单
      let sql = `insert into sy_orders values`
      let addressObj = data[1][0]
      let u_name = addressObj.name
      let u_phone = addressObj.tel
      let u_address = addressObj.province + '' + addressObj.city + addressObj.county
      // 拼接sql
      data[0].forEach((goods, i) => {
        // sql += `(null,'${goods.title}','${goods.store}','${u_name}','${u_phone}','${u_address}', ${status}, ${u_id}),`
        sql += `(not null,'${goods.store}','${goods.title}','${goods.img}',${goods.price},${totalArr[i] -0}, ${u_id},'${u_name}','${u_phone}','${u_address}', ${status}),`
      })
      sql = sql.substring(0, sql.length - 1) + ';'
      // 添加订单
      db.query(sql, (err2,data) => {
        // console.log(data);
        // console.log(err2);
        if (err2) {
          res.json({
            status: 400,
            msg: '订单添加失败！'
          })
          return
        }
        res.json({
          status: 200
        })
      })
    })

  } catch {
    res.json({
      status: 400,
      error: '用户id错误！'
    })
  }
})

// 获取订单
router.get('/orders', (req, res) => {
  let token = req.headers.authorization
  try {
    takeId(token)
    let u_id = id // 用户id
    let status = req.query.status
    db.query(`select id,g_title,g_price,g_totalnum,g_img,g_store from sy_orders where u_id = ${u_id} and status in (${status})`, (err, result) => {
      res.json({
        status: 200,
        data: result
      })
    })
  } catch (err) {
    res.json({
      status: 400,
      error: '用户id错误！'
    })
  }
})

module.exports = router